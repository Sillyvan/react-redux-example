const initialState = {
  items: ['test', 'test2'],
};

const itemHandler = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      return {
        ...state,
        items: [...state.items, action.item],
      };
    default:
      return state;
  }
};

export default itemHandler;
