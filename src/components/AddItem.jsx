import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

export const AddItem = () => {
  const [input, setInput] = useState('');
  const handleChange = e => {
    setInput(e.target.value);
  };
  const dispatch = useDispatch();
  return (
    <div className='center'>
      <h1>Add Item</h1>
      <input type='text' value={input} onChange={handleChange} />
      <button onClick={() => dispatch({ type: 'ADD_ITEM', item: input })}>
        Add Item
      </button>
    </div>
  );
};
