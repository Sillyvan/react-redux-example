import React from 'react';
import { useSelector } from 'react-redux';

export const ShowItems = () => {
  const items = useSelector(state => state.items);

  const allItems = () => {
    return items.map(item => (
      <h2 className='list' key={item}>
        {item}
      </h2>
    ));
  };
  return (
    <>
      <div className='center'>
        <h1>All Items:</h1>
        <div>{allItems()}</div>
      </div>
    </>
  );
};
