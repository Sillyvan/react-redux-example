import React from 'react';
import { AddItem } from './components/AddItem';
import { ShowItems } from './components/ShowItems';

const App = () => {
  return (
    <>
      <AddItem />
      <ShowItems />
    </>
  );
};

export default App;
